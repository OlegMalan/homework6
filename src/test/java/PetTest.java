import HeppyFamily1.Pet;
import HeppyFamily1.Species;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Objects;

import static HeppyFamily1.Species.CAT;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class PetTest {
    static Pet pet = new Pet(CAT,"Myrca",1,33,new String[]{"eat","slip"});



    @Test
    public void eatTest(){
        System.out.println("Я ї'м!");
    }
    @Test
    public  void respondTest(){
        System.out.println("Привіт, хазяїн. Я - " + pet.getNickname() + ". Я скучив!");
    }
    @Test
    public void foul() { System.out.println("Потрібно добре замести сліди..."); }

    @Test
    public void toStringTest(){

        assertEquals("Cat{nickname='Myrca', age=1, trickLevel=33, habits=[eat, slip]}",pet.toString());
    }
    @Test
    public void equalsTest(){
        assertEquals(pet.getSpecies().equals(pet.getSpecies()) &&
                pet.getNickname().equals(pet.getNickname()) &&
                pet.getAge() == pet.getAge() &&
                pet.getTrickLevel() == pet.getTrickLevel() &&
                Arrays.equals(pet.getHabits(), pet.getHabits()),pet.equals(pet));
    }

    @Test
    public void hashCodeTest(){
        int res = (31*(Objects.hash(pet.getSpecies(), pet.getNickname(), pet.getAge(), pet.getTrickLevel()))+ Arrays.hashCode(pet.getHabits()));
        assertEquals(res,pet.hashCode());
    }



}
