import HeppyFamily1.Family;
import HeppyFamily1.Human;
import HeppyFamily1.Pet;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.ref.Reference;
import java.lang.ref.WeakReference;
import java.util.Arrays;
import java.util.Objects;

import static HeppyFamily1.Species.CAT;
import static HeppyFamily1.Species.DOG;
import static org.junit.jupiter.api.Assertions.assertNull;

public class FamilyTest {
    private Human[] children ;

@Test
    public void addChildTest() {
        Human child = new Human();

        if (children == null) {
            children = new Human[0];
        }
        Human[] newChildren = new Human[children.length + 1];
        System.arraycopy(children, 0, newChildren, 0, children.length);
        newChildren[newChildren.length - 1] = child;
        children = newChildren;
        child.setFamily(this.children);
    }

    @Test
    public void deleteChildTest() {
        Human p = new Human();
        Human c1 = new Human();
        Human c2 = new Human();
        Family f = new Family(p,c1,c2);
        f.deleteChild(1);
    }
    @Test
    public void countFamilyTest() {
        Human p = new Human();
        Human c1 = new Human();
        Human c2 = new Human();
        Family f = new Family(p,c1,c2);

        f.countFamily();
    }

     @Test
     public void toStringTest(){
         Human mother = new Human();
         Human father = new Human();
         StringBuilder sb = new StringBuilder();
         sb.append("Family:").append("\n");
         sb.append("  Mother: ").append(mother).append("\n");
         sb.append("  Father: ").append(father).append("\n");
         sb.append("  Children:").append("\n");
         sb.toString();
     }
    @Test
    public void equalsTest() {
        Human mother = new Human();
        Human father = new Human();
        Human child = new Human();
        Family f = new Family(mother,father,child);

        f.setMother(mother);
        f.setFather(father);

        f.addChild(child);
        Family f2 = new Family(mother,father,child);
        f2.setMother(mother);
        f2.setFather(father);

        f2.addChild(child);

        Assertions.assertTrue(f.equals(f2));
    }
//
    @Test
    public void hashCodTest() {
        Human mother = new Human();
        Human father = new Human();
        Human child = new Human();
        Pet p = new Pet();
        Family f = new Family(mother,father,p,child);
        int result = ((Objects.hash(f.getMother(),f.getMother(),f.getPet()))*31)+Arrays.hashCode(f.getChildren());
        Assertions.assertEquals(result,f.hashCode());
    }
    @Test
    public void testFinalize() throws Exception {

        Human mother = new Human();
        Human father = new Human();
        Human child = new Human();
        Family family = new Family(mother, father, child);
        Reference<Family> familyRef = new WeakReference<>(family);
        System.gc();
        int i = 0;
        while (familyRef.get() != null && i < 100) {
            Thread.sleep(100);
            i++;
        }
        if (familyRef.get() != null) {
            System.err.println("**Об'єкт Family не видалено!**");
        }
}


}
