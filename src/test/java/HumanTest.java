import HeppyFamily1.Human;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class HumanTest {
    static Human humanC = new Human("Oleg", "Malanchuk", 2005, 130, "Maria Malanchuk", "Volodymyr Malanchuk", PetTest.pet);
    static Human humanC2 = new Human("Roman", "Malanchuk", 2012, 9, "Maria Malanchuk", "Volodymyr Malanchuk", PetTest.pet);
//


    @Test
    public void greetPet(){
        System.out.println("Привіт, "+PetTest.pet.getNickname());
    }
    @Test
    public void describePet(){
        if ((PetTest.pet) !=null) {
            String trick = PetTest.pet.getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий";
            System.out.println("У мене є " + PetTest.pet.getSpecies() + ", їй " + PetTest.pet.getAge() + " років, він " + trick + ".");
        } else {
            System.out.println("У мене немає домашнього улюбленця.");
        }
    }
    @Test
    public void toStringTest(){
        assertEquals("Malanchuk{ name='Oleg', year=2005, ip=130, mather=null,father=null,pet=nullschedule=null}",humanC.toString());
    }
    @Test
    public void equalsTest(){
        assertEquals(humanC.getSurname().equals(humanC.getSurname()) &&
                humanC.getName().equals(humanC.getName()) &&
                humanC.getYear() == humanC.getYear() &&
                humanC.getIq() == humanC.getIq() &&
                humanC.getMather()== humanC.getMather()&&
                humanC.getFather()== humanC.getFather()&&
                humanC.getPet()== humanC.getPet()&&
                Arrays.equals(humanC.getSchedule(),humanC.getSchedule()),humanC.equals(humanC));
    }

    @Test
    public void hashCodeTest(){
        int result = ((Objects.hash(humanC.getName(), humanC.getSurname(), humanC.getYear(), humanC.getIq() , humanC.getMather(),humanC.getFather(), humanC.getPet()))*31)+Arrays.hashCode(humanC.getSchedule());
        assertEquals(result,humanC.hashCode());
    }

    @Test
    public void testFinalize() throws Exception {
        int initialAliveCount = Human.getAliveCount();
        humanC = null;
        humanC2= null;
        System.gc();
        assertEquals(initialAliveCount , Human.getAliveCount());
    }


}
