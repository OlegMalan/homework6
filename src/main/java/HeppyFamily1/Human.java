package HeppyFamily1;
import java.util.HashMap;



import java.util.*;



public class Human {
    private static int aliveCount = 0;
    private String name;
    private String surname;
    private int year;
    private  String mather;
    private  String father;
    private Pet pet;
    private int iq;
    private Map<DayOfWeek, String> schedule = new HashMap<>();






    public Human(String name, String surname, int year,int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        aliveCount++;

    }

    public  Human(String name, String surname, int year, int iq, String mather, String father, Pet pet) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.mather = mather;
        this.father = father;
        this.pet=pet;
        aliveCount++;


    }
    public Human(String name, String surname, int year, int iq, Pet pet, Map<DayOfWeek, String> schedule) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.schedule = schedule;
        aliveCount++;
    }
    public Human() {

    }

    public static int  getAliveCount() {
        return aliveCount;
    }

    public void greetPet(){


        System.out.println("Привіт, "+pet.getNickname());
    }

    public void describePet(){
        if ((pet) !=null) {
            String trick = pet.getTrickLevel() > 50 ? "дуже хитрий" : "майже не хитрий";
            System.out.println("У мене є " + pet.getSpecies() + ", їй " + pet.getAge() + " років, він " + trick + ".");
        } else {
            System.out.println("У мене немає домашнього улюбленця.");
        }
    }

    @Override
    public String toString() {
        return surname + "{" +
                " name='" + name + '\'' +
                ", year=" + year +
                ", ip=" + iq +
                ", mather=" +mather+
                ",father="+father+
                ",pet="+pet+
                "schedule="+Arrays.deepToString(new Map[]{schedule})+
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
       Human human = (Human) o;
        return name.equals(human.name) &&
                surname.equals(human.surname) &&
                year == human.year &&
                iq == human.iq &&
                mather == human.mather&&
                father == human.father&&
                pet == human.pet&&

                Arrays.equals(new Map[]{schedule}, new Map[]{human.schedule});
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(name, surname, year, iq , mather,father, pet);
        result = 31 * result + Arrays.hashCode(new Map[]{schedule});
        return result;
    }


    @SuppressWarnings("removal")
    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        aliveCount--;
        System.out.println("Видалено об'єкт Human: " + this);
    }
    public void addSchedule(DayOfWeek dayOfWeek, String activity) {
        schedule.put(dayOfWeek, activity);
    }




    public int getYear() {
        return year;
    } public int getIq() {
        return iq;
    }

    public Map[] getSchedule() {
        return new Map[]{schedule};
    }

    public String getMather() {
        return mather;
    }

    public String getFather() {
        return father;
    }

    public Pet getPet() {
        return pet;
    }
    public void setSurname(String surname) {
        this.surname = surname;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }




    public void setPet(Pet pet) {
    }


    public void setFamily(Family family) {
    }

    public void setFamily(Human[] children) {
    }


    public void addChild(Human child1) {
    }


    public void setFather(Human father) {
    }

    public void setMother(Human mother) {
    }
}
